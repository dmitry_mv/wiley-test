(function (angular) {
    'use strict';

    angular.module('wileyApp', ['app.core']);

})(window.angular);
(function (angular) {
    'use strict';

    angular.module('itemFactory', [])
        .factory('itemFactory', factoryService);

    factoryService.$inject = [];
    function factoryService() {

        function Item(obj) {
            if (Object.keys(obj).length > 0) {
                this.title = obj.title;
                this.status = angular.isString(obj.status) ? (obj.status == "true") : obj.status;
                this.id = obj.id || Date.now();
            } else {
                this.title = '***';
                this.status = false;
                this.id = Date.now();
            }
        }

        Item.prototype = {};

        return ( Item );

    }


})(window.angular);

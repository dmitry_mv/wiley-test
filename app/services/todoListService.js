(function (angular) {
    'use strict';

    angular.module('todoListService', [])
        .service('todoListService', Service);

    Service.$inject = ['localStorageService', 'itemFactory'];
    function Service(localStorageService, itemFactory) {

        var self = this;

        self.list = [];

        self.initStorage = function () {
            var storage = localStorageService.getObject('todoList');
            if (Object.keys(storage).length > 0) {
                storage.list.forEach(function (item) {
                    self.list.push(new itemFactory(item));
                });
            } else {
                localStorageService.setObject('todoList', {list: []});
            }
        };
        self.addItem = function (title) {
            var model = new itemFactory({title: title, status: false});
            self.list.push(model);
            self.saveStorageList();
        };
        self.removeItem = function (id) {
            for (var i = 0; i < self.list.length; i++) {
                if (self.list[i]['id'] == id) {
                    self.list.splice(i, 1);
                    break;
                }
            }
            self.saveStorageList();
        };
        self.submitItem = function (item) {
            if (item.newValue != '') {
                item.title = item.newValue;
                item.isEdit = false;
                self.saveStorageList();
            }
        };
        self.toggleStatus = function (item) {
            item.status = !item.status;
            self.saveStorageList();
        };
        self.saveStorageList = function () {
            localStorageService.setObject('todoList', {list: self.list});
        };
        self.removeSelectedItem = function () {
            var arrIds = [];
            self.list.forEach(function (item) {
                if (item.status) {
                    arrIds.push(item.id);
                }
            });
            arrIds.forEach(function (id) {
                self.removeItem(id);
            });
        };

        self.initStorage();

        return self;


    }

})(window.angular);
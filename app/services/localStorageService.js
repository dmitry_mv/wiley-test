(function (angular) {
    'use strict';

    angular.module('localStorageService', [])
        .factory('localStorageService', localStorageService);

    localStorageService.$inject = ['$window'];
    function localStorageService($window){
        return {
            set: set,
            get: get,
            setObject: setObject,
            getObject: getObject
        };
        function set(key, value){
            $window.localStorage[key] = value;
        }
        function get(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        }
        function setObject(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        }
        function getObject(key){
            return JSON.parse($window.localStorage[key] || '{}');
        }

    }

})(window.angular);
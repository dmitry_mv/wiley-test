(function (angular) {
    'use strict';

    angular.module('core.services', [
        'localStorageService',
        'todoListService'
    ]);

})(window.angular);

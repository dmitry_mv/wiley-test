(function (angular) {
    'use strict';

    angular.module('app.core', [
        //our modules
        //'constants.core',
        'core.directives',
        'core.services',
        //'core.filters',
        'core.factories'
    ]);
})(window.angular);

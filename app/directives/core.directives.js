(function(angular) {
    'use strict';

    angular.module('core.directives', [
       'todoList'
    ]);

})(window.angular);

(function (angular) {
    'use strict';

    var moduleName = 'todoList';

    angular.module(moduleName, [])
        .directive(moduleName, DirectiveController);

    DirectiveController.$inject = ['todoListService'];
    function DirectiveController(todoListService) {

        var directive = {
            restrict: 'EA',
            templateUrl: 'app/directives/' + moduleName + '/tpl.html',
            scope: {},
            link: linkFunc,
            controller: Controller,
            controllerAs: 'vm'
        };

        Controller.$inject = [];
        return directive;

        function Controller() {
            var vm = this;
            vm.todoListService = todoListService;
            vm.submit = function(form){
                if (form.$valid){
                    vm.todoListService.addItem(vm.taskName);
                    vm.taskName = '';
                }
            };
            vm.taskName = '';
        }

        function linkFunc(scope, el, attr, ctrl) {

        }
    }


})
(window.angular);

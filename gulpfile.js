var gulp = require('gulp'),
    ngHtml2Js = require("gulp-ng-html2js"),
    minifyHTML = require('gulp-minify-html'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    rev = require('gulp-rev'),
    uglify = require('gulp-uglify');

gulp.task('p-ht', function () {
    var opts = {
        conditionals: true,
        spare: true
    };
    return gulp.src('src/html/index.html')
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest('dest/html/'))
});

gulp.task('custom_partials', function () {

    var opts = {
        empty: true,
        spare: true,
        quotes: true
    };

    return gulp.src(
        [
            'app/directives/**/**/*.html',
            'app/views/**/**/*.html'
        ],
        {base: 'app/'}
        )
        .pipe(minifyHTML(opts))
        .pipe(ngHtml2Js({
            moduleName: "MyAwesomePartials",
            prefix: "app/"
        }))
        .pipe(concat("partials.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest('bundle/js'));

    //.pipe(concat('templates.js'))
    //.pipe(rename({suffix: '.min'}))
    //.pipe(gulp.dest('bundle/js'));

});

gulp.task('html-templates-build', function () {

    var opts = {
        empty: true,
        spare: true,
        quotes: true
    };

    return gulp.src(
        [
            'app/directives/**/**/*.html',
            'app/views/**/**/*.html'
        ],
        {base: 'app/'}
        )
        .pipe(minifyHTML(opts))
        .pipe(ngHtml2Js({
            moduleName: "MyAwesomePartials",
            prefix: "app/"
        }))
        .pipe(concat("partials.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest('bundle/js'));

    //.pipe(concat('templates.js'))
    //.pipe(rename({suffix: '.min'}))
    //.pipe(gulp.dest('bundle/js'));

});

//build static
gulp.task('css-build-static', function () {
    return gulp.src('app/libs/common/css/*.css')
        .pipe(autoprefixer('last 2 version'))
        .pipe(minifycss())
        .pipe(concat('kaminus-static.css'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest('bundle/css'))
});
gulp.task('js-compress-custom', function () {
    return gulp.src(
        [
            //'app/libs/vendor/js/mask.js'
            //'app/libs/vendor/js/jquery.ez-plus.js'
            'app/libs/vendor/js/angular-ezplus.js'
        ],
        {base: 'app/'}
        )
        .pipe(uglify())
        .pipe(concat('angular-ezplus.js'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest('bundle/js'));

});
gulp.task('js-build-static', function () {
    return gulp.src(
        [
            'app/libs/common/js/jquery.min.js',
            'app/libs/common/js/angular.min.js',
            'app/libs/common/js/angular-animate.min.js',
            'app/libs/common/js/angular-aria.min.js',
            'app/libs/common/js/angular-messages.min.js',
            'app/libs/common/js/angular-material.min.js',
            'app/libs/common/js/angular-route.min.js',
            'app/libs/common/js/owl.carousel.min.js',
            'app/libs/common/js/underscore-min.js',
            'app/libs/common/js/angular.img-fallback.min.js',
            //'app/libs/common/js/angular-material-icons.min.js',
            'app/libs/common/js/angular-sanitize.min.js',
            //'app/libs/common/js/angular-disqus.min.js',
            //'app/libs/common/js/angular-socialshare.min.js',
            'app/libs/common/js/angular-metatags.min.js',
            'app/libs/common/js/mask.min.js',
            'app/libs/common/js/ng-infinite-scroll.min.js',
            'app/libs/common/js/rzslider.min.js',

            'app/libs/common/js/angular-ezplus.min.js',
            'app/libs/common/js/jquery.ez-plus.min.js'
        ],
        {base: 'app/'}
        )
        //.pipe(uglify())
        .pipe(concat('kaminus-static.js'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest('bundle/js'));

});

//sass compress
gulp.task('scss-compress', function () {
    return gulp.src('app/libs/scss/**/**/*.scss')
        .pipe(sass({style: 'expanded'}))
        //.pipe(autoprefixer('last 2 version'))
        //.pipe(gulp.dest('dest/styles/'))
        //.pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(concat('kaminus-dynamic.css'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest('bundle/css'))
});
//js compress
gulp.task('js-compress', function () {
    return gulp.src(
        [
            'app/app.module.js',
            'app/app.routes.js',
            'app/core/core.module.js',
            'app/constants/*.js',
            'app/controllers/*.js',
            'app/directives/**/**/*.js',
            'app/services/**/**/*.js',
            'app/filters/**/**/*.js',
            'app/factories/**/**/*.js'
            //'app/services/*.js',
        ],
        {base: 'app/'}
        )
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest('bundle/js'));

});

//FINAL METHODS
//**************************************************
gulp.task('js-build-final', function () {
    return gulp.src(
        [
            'bundle/js/kaminus-static.min.js',
            'bundle/js/partials.min.js',
            'bundle/js/app.min.js'
        ],
        {base: 'app/'}
        )
        .pipe(concat('k-final.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('bundle/final'));
});
gulp.task('css-build-final', function () {
    return gulp.src(
        [
            'bundle/css/kaminus-static.min.css',
            'bundle/css/kaminus-dynamic.min.css'
        ],
        {base: 'app/'}
        )
        .pipe(concat('k-final.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('bundle/final'))
});

//**************************************************

//for sass auto compile
gulp.task('watch', function () {
    //gulp.watch('app/libs/scss/**/*.scss', ['scss-compress']);
    gulp.watch([
        'app/app.module.js',
        'app/app.routes.js',
        'app/core/core.module.js',
        'app/constants/*.js',
        'app/controllers/*.js',
        'app/directives/**/**/*.js',
        'app/services/**/**/*.js',
        'app/filters/**/**/*.js',
        'app/factories/**/**/*.js'
    ], ['js-compress']);

    //gulp.watch([
    //    'app/directives/**/**/*.html',
    //    'app/views/**/**/*.html'
    //], ['html-templates-build'])

});

gulp.task('dev', ['watch'], function () {
});

gulp.task('build-static', ['css-build-static', 'js-build-static'], function () {
});

gulp.task('build-final', ['css-build-final', 'js-build-final'], function () {
});

